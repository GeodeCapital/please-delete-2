.<-suppressMessages({library(gma)})
options(stringsAsFactors=FALSE, warn=1)
dow <- g.dow()
if (dow=="Thu" && g.date.idx( ,T,-1,cal="cal") %ni% dates.usd) dow <- "Wed"
if (dow != "Wed") q()
## mf.trade(fund="MANFUT")              # Real
## mf.trade(fund="MF")                  # Chalk
## mf.trade(fund="MFINF")               # Chalk
