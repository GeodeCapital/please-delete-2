suppressMessages({library(gma); library(bbdotnet3); library(sand)})
options(stringsAsFactors=FALSE, warn=1)
pod <- tryPod(PODS)
ddir <- g.mk.latest(GMA.DATA.DIR, under.data=FALSE, depth=3, latest=TRUE)

## Commodity Signal: 100 * (C1/C2-1) / AnnFactor

load(g.p("$ddir/comdty.RData"))                                 # Time series of commodity prices
mr.comdty <- subset(comdty, g.date8(date)==max(g.date8(date)))
mr.comdty <- mr.comdty[with(mr.comdty, order(comdty, cnum)), ]
## TO FIX: we need to insure that each commodity has only 2 legs

contract.lag <- tapply(mr.comdty$expiry, mr.comdty$comdty, function(x){
    y <- as.Date("01 " %&% x, "%d %b %y")
    res <- as.numeric(diff(y))
    if(!length(res)) NA else res
})
prc.ratios    <- tapply(mr.comdty$price,  mr.comdty$comdty, function(x) x[1] / x[2] - 1)
annual.adj    <- pmax(contract.lag / 360, 0.1, na.rm=TRUE)
comdty.signal <- prc.ratios / annual.adj * 100

## Currency Signal: int.rate

load(g.p("$ddir/curncy.RData"))                                 # Time series of commodity prices
mr.curncy <- subset(curncy, g.date8(date)==max(g.date8(date)) & !is.na(i1))
curncy.signal <- structure(mr.curncy$i1, names=mr.curncy$curncy)

signal <- list(comdty=comdty.signal, curncy=curncy.signal)
save(signal, file=g.p("$ddir/gma.signal.RData"))
