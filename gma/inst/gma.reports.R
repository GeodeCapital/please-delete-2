.<-suppressMessages({library(gma); library(sand); library(report); library(bbdotnet3)})
options(stringsAsFactors=FALSE, warn=1)
setwd(ddir <- m.rdd.path(g.date8(), GMA.DATA.DIR))
yest <- g.date.idx( ,T,-1)
mstd <- g.date.idx(yest, T, cal="mnth.beg", later=F)

## g.printer(file="gc5.heatmap.pdf")
## gma.heatmap()
## dev.off()

## g.printer(margins=c(-2,0,1,0,0), file="mp.tnamap.pdf")
## mp.tnamap()
## dev.off()

## fn <- "gc5.altrpt.pdf"
## g.printer(file=fn)
## g.plot.init();  text(.5, .5, "No gc5.altrpt", cex=2)
## gma.alt.rpt()
## dev.off()
## .<-file.copy(fn, "/gcm/strategies/stage/cio/GeodeDailyReports/dropoff", TRUE)

g.printer(file="sandity.pdf")
gma.sandity()
dev.off()
q()

## g.printer(margins=c(-2,0,1,0,0), file="MPEQVAL.Attr.pdf")
## g.plot.init();  text(.5, .5, "No MPEQVAL.Attr", cex=2)
## g.attrib("MPEQVAL", mstd, yest, alphaFile=NULL)
## dev.off()

## g.printer(margins=c(-2,0,1,0,0), file="MPEQDEF.Attr.pdf")
## g.plot.init();  text(.5, .5, "No MPEQDEF.Attr", cex=2)
## g.attrib("MPEQDEF", mstd, yest, alphaFile=NULL)
## dev.off()

## mp.hldg.rpt("MP")                                   # Uses g.rpt.tex, so no g.printer() needed

## g.printer(file="mp.hldg.sum.pdf")
## g.plot.init();  text(.5, .5, "No mp.hldg.sum", cex=2)
## mp.hldg.sum("MP")
## dev.off()

## g.printer(margins=c(-2,0,1,0,0), file="mp.attr.d1.pdf")
## mp.attr.rpt("MP", perd="d1")
## dev.off()
## g.printer(margins=c(-2,0,1,0,0), file="mp.attr.d5.pdf")
## mp.attr.rpt("MP", perd="d5")
## dev.off()
## g.printer(margins=c(-2,0,1,0,0), file="mp.attr.mtd.pdf")
## mp.attr.rpt("MP", perd="mtd")
## dev.off()
## if (g.date8() == g.date.idx( , T, cal="qtr.beg", later=F)) {
##     g.printer(margins=c(-2,0,1,0,0), file="mp.attr.qtd.pdf")
##     mp.attr.rpt("MP", perd="qtd")
##     dev.off()
## }

## GMAP:
ddir <- g.mk.latest(file.path(GMA.DATA.DIR, "gmap"), under.data=F, depth=3, latest=T)
setwd(ddir)

g.printer(file="gmap.heatmap.pdf")
gma.heatmap(base="GM")
dev.off()

g.printer(margins=c(-2,0,1,0,0), file="gmap.tnamap.pdf")
mp.tnamap(base="GM")
dev.off()

## g.printer(margins=c(-2,0,1,0,0), file="GMEQVAL.Attr.pdf")
## g.attrib("GMEQVAL", mstd, yest, alphaFile=NULL)
## dev.off()

## g.printer(margins=c(-2,0,1,0,0), file="GMEQDEF.Attr.pdf")
## g.attrib("GMEQDEF", mstd, yest, alphaFile=NULL)
## dev.off()

mp.hldg.rpt("GM")                                      # Uses g.rpt.tex, so no g.printer() needed

g.printer(file="gm.hldg.sum.pdf")
mp.hldg.sum("GM")
dev.off()

g.printer(margins=c(-2,0,1,0,0), file="gm.attr.d1.pdf")
mp.attr.rpt("GM", perd="d1")
dev.off()
g.printer(margins=c(-2,0,1,0,0), file="gm.attr.d10.pdf")
mp.attr.rpt("GM", perd="d10")
dev.off()
g.printer(margins=c(-2,0,1,0,0), file="gm.attr.mtd.pdf")
mp.attr.rpt("GM", perd="mtd")
dev.off()
if (g.date8() == g.date.idx( , T, cal="qtr.beg", later=F)) {
    g.printer(margins=c(-2,0,1,0,0), file="gm.attr.qtd.pdf")
    mp.attr.rpt("GM", perd="qtd")
    dev.off()
}

## UMAP:
ddir <- g.mk.latest(file.path(GMA.DATA.DIR, "umap"), under.data=F, depth=3, latest=T)
setwd(ddir)

## g.printer(file="umap.heatmap.pdf")
## gma.heatmap(base="UC")
## dev.off()

## g.printer(margins=c(-2,0,1,0,0), file="umap.tnamap.pdf")
## mp.tnamap(base="UC")
## dev.off()

## mp.hldg.rpt("UC")                                      # Uses g.rpt.tex, so no g.printer() needed

## g.printer(file="uc.hldg.sum.pdf")
## mp.hldg.sum("UC")
## dev.off()

## g.printer(margins=c(-2,0,1,0,0), file="uc.attr.d1.pdf")
## mp.attr.rpt("UC", perd="d1")
## dev.off()
## g.printer(margins=c(-2,0,1,0,0), file="uc.attr.d5.pdf")
## mp.attr.rpt("UC", perd="d5")
## dev.off()
## g.printer(margins=c(-2,0,1,0,0), file="uc.attr.mtd.pdf")
## mp.attr.rpt("UC", perd="mtd")
## dev.off()
## if (g.date8() == g.date.idx( , T, cal="qtr.beg", later=F)) {
##     g.printer(margins=c(-2,0,1,0,0), file="gm.attr.qtd.pdf")
##     mp.attr.rpt("UC", perd="qtd")
##     dev.off()
## }
