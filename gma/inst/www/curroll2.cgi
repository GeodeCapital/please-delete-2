#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";
foreach $pair (split(/&/, <>)) {
  ($key, $val) = split(/=/, $pair, 2);
  $in{$key} .= $val;
}
if (crypt($in{pwd},"db") ne "db/HmZh8z2lsg") {print "Wrong password.\n"; exit};

require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");
print SCRIPT <<EOF;
  expy <- "$in{expy}"
EOF
print SCRIPT <<'EOF';                                                            # Begin R script
.<-suppressMessages({require(gma); require(sand); require(chalk)})
options(stringsAsFactors=FALSE, warn=1)

## DMAP, GMAP, UMAP:
bid <- basketId()
for (port in c(t(outer(c("MP","GM","UC") %&% "FX", c("VAL","CAR","MOM","DEF","COT"), paste0)))) {
    yo <- yn <- sandHoldings(port)[c("symbol","shares")]
    if (nrow(yo)==0) next
    yo$shares <- -yo$shares                                              # Sell the old positions
    yn$symbol <- sub("[0-9]+", expy, yn$symbol)             # Enter new positions with new expiry
    yn$symbol[yn$symbol=="IDRUSD123020.MS"] <- "IDRUSD122320.MS"
    sandTrd(rbind(yo,yn), port, instructions="MKT", basketid=bid)
    cat(port, "done.\n")
}
sandPlaceTrades(bid)

## Chalk funds:
for (fund in c("MF","MFINF","CAR","CARINF")) {
    yo <- yn <- subset(chalkHoldings(fund)[c("symbol","shares")], substr(symbol,13,15)==".MS")
    yo$shares <- -yo$shares                                              # Sell the old positions
    yn$symbol <- sub("[0-9]+", expy, yn$symbol)             # Enter new positions with new expiry
    yn$symbol[yn$symbol=="IDRUSD123020.MS"] <- "IDRUSD122320.MS"
    y <- rbind(yo,yn)
    chalkTrade(fund, y$symbol, y$shares)
    cat(fund, "done.\n")
}

EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
