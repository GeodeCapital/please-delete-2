#!/usr/bin/perl
$| = 1;				                     # Don't buffer Perl output
print "Content-type: text/html\n\n<PRE>\n";

require("/gcm/gcm_sys2home/gcmweb/.stuff.pl");                                # Linux environment
$script = "/tmp/rwrap$$.R";
open(SCRIPT, ">$script");

print SCRIPT <<'EOF';                                                            # Begin R script
.<-suppressMessages(library(sand))

h <- sandHoldings("GMEQVOL")	#  Was MPEQVOL
x <- g.rparse(h$symbol, "-")$ext
h <- subset(h, as.integer(paste0("20", substr(x, 5,6), substr(x, 1,4)))==g.date8())
if (!nrow(h)) {cat("No options expire today.\n"); q()}
idxs <- c("NKY.","HSI.","AS51","SPX.","UKX.","SX5E","WSX5","SMI.","SPXW")            # Time order
i <- match(substr(h$symbol,1,4), idxs)
h <- h[i==min(i), ]                                             # Choose just one index to handle
rownames(h) <- NULL
print(h[c("symbol","sec_id","curncy","shares")])
h$cp <- substr(g.parse(h$symbol)$ext, 1,1)
idc1 <- min(h$sec_id[h$cp=="C"])
idc2 <- max(h$sec_id[h$cp=="C"])
idp1 <- min(h$sec_id[h$cp=="P"])
idp2 <- max(h$sec_id[h$cp=="P"])

cat("<form method=POST action=expire2.cgi>",
    " <input type=hidden name=idc1 value=", idc1, ">",
    " <input type=hidden name=idc2 value=", idc2, ">",
    " <input type=hidden name=idp1 value=", idp1, ">",
    " <input type=hidden name=idp2 value=", idp2, ">",
    " Please enter settlement price: <input name=sprc size=8>",
    " and password: <input type=password name=pwd size=3> &nbsp",
    " <input type=submit value='Go!'> </form>", sep="")

EOF
close(SCRIPT);			           # End R script
system("/gcm/gcm_sys2home/gcmweb/local/bin/R --vanilla --slave < $script 2>&1");
$? && die "Aborted in R with errcode $?";   # Check for successful R completion
unlink $script;
