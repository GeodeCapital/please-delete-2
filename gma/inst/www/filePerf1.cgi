#!/usr/bin/perl

print "Content-type: text/html\n\n";
print <<EOF;
  <TITLE>File-Based Performance Stats</TITLE>
  <A HREF=/><IMG align=right border=0 src=/logo.gif></A>
  <H1 ALIGN="CENTER">File-Based Performance Stats</H1>
  Assumes a tab-delimited text file
    /gcm/gcm_sys2home/gcmweb/apache/htdocs/DB/Input/perf.txt.<BR>
  Note /gcm/gcm_sys2home is the I: drive (\\\\gcmnasp\\gcm_sys2home) on Windows.<BR>
  First row should be a header.  First column should be date, the rest daily or monthly returns.<P>
EOF

$count = 0;
open (IN, "/gcm/gcm_sys2home/gcmweb/apache/htdocs/DB/Input/perf.txt") || die "No file\n";
while (<IN>) {
  chop;
  @s = split (/\t/, $_);
  $endt = shift @s;
  if ($count==0) {@hdr=@s}
  $count++;
}
$opts1 = join(" ", map("<OPTION>$_</OPTION>", @hdr));
$sz = 5;
if (scalar(@hdr) < 5) {$sz = @hdr}

print <<EOF;
  <form method="POST" action="filePerf2.cgi">
  Password:  <input type=password name=pwd size=3> &nbsp
  Fund:      <select name=fund size=$sz> $opts1 </select> &nbsp
  End:       <input name=endt size=10 value='$endt'> &nbsp &nbsp
  PNG?:      <input name=png type=checkbox> &nbsp &nbsp
  <input type=submit value="Go!">
  </form>
  <HR><EM> David E. Brahm
  (<A HREF="mailto:David.Brahm\@geodecapital.com"> David.Brahm\@geodecapital.com </A>)
  &nbsp Last update 4/12/18
EOF
