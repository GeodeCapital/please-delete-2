suppressMessages({library(gma); library(bbdotnet3)})
options(stringsAsFactors=FALSE, warn=-1, useDates=FALSE)   # No Date objects from getBbHistorical
ddir <- g.mk.latest(GMA.DATA.DIR, under.data=FALSE, depth=3, latest=TRUE)
endt <- g.date.idx( , TRUE, -1)

pod <- tryPod(PODS)
if (identical(pod, FALSE)) stop("Could not connect to any pods!")

## CURRENCY:
y <- db.sql("SELECT a.curncy, a.i1_index, b.[date]",       # By currency, what's the latest data?
            "FROM gma..curncy_info a LEFT JOIN",
            " (SELECT [curncy], max([date]) [date] FROM gma..curncy GROUP BY curncy) b",
            "  ON a.curncy = b.curncy")
y <- subset(y, date < endt) 

## Get most recent PPP and GAP data.
y.ppp <- db.sql("SELECT a.curncy, a.date, a.ppp ",
                "  FROM gma..curncy a JOIN",
                "   (SELECT curncy, max(date) date FROM gma..curncy WHERE ppp IS NOT NULL",
                "    GROUP BY curncy) b",
                "  ON a.curncy = b.curncy AND a.date = b.date ORDER BY a.curncy")

y.gap <- db.sql("SELECT a.curncy, a.date, a.gap ",
                "  FROM gma..curncy a JOIN",
                "   (SELECT curncy, max(date) date FROM gma..curncy WHERE gap IS NOT NULL",
                "    GROUP BY curncy) b",
                "  ON a.curncy = b.curncy AND a.date = b.date ORDER BY a.curncy")

map.ppp <- c(USD="US", CAD="CA", JPY="JN", EUR="EUAR", NOK="NO",
             SEK="SW", AUD="AS", NZD="NZ", GBP="UK", CHF="SZ",
             BRL="BZ", COP="CO", CZK="CZ", HUF="HG", IDR="ID", ILS="IL",
             INR="IN", KRW="KO", MXN="MX", PLN="PO", TRY="TK", ZAR="ZA")
map.5yr <- c(USD="USGG5YR", CAD="GCAN5YR", JPY="GJGB5",   EUR="GDBR5",   NOK="GNOR5YR",
             SEK="GSGB5YR", AUD="GACGB5",  NZD="GNZGB5",  GBP="GUKG5",   CHF="GSWISS05",
             DKK="GDGB5YR", HKD="GHKGB5Y", ILS="GISR5YR", SGD="MASB5Y",  CZK="CZGB5YR",
             HUF="GHGB5YR", MXN="VRTBT5",  PLN="POGB5YR", ZAR="GSAB5YR")
map.10y <- sub("0*5", "10", map.5yr)
map.gap <- c(USD="EOUSG001", CAD="EOCAG001", JPY="EOJPG001", EUR="E23EG001",
             NOK="EONOG001", SEK="EOSEG001", AUD="EOAUG001", NZD="EONZG001",
             GBP="EOUKG001", CHF="EOCHG001")

## Function to get yields but only if "time" is current, copied from gma.curtrd.R:
cyld <- function(idxs) {
    x <- getBbRealTime(c("last_price","time"), idxs, "Index", pod)
    yld <- as.double(x[ ,1])
    ydts <- rep(g.date8(), length(yld))
    if (any(q <- g.dflt(nchar(x[ ,2]),2)==10)) ydts[q] <- g.read.date(x[q,2])
    yld[g.date.idx() - g.date.idx(ydts) > 1] <- NA
    yld
}

for (i in 1 %:% nrow(y)){
    cat("[", curncy <- y$curncy[i], "] ", sep="")
    int.rate <- y$i1.index[i]
    vol      <- curncy %&% "V1M"
    ppp      <- as.vector(g.dflt("PPP " %&% map.ppp[curncy], "PPP"))
    tres.5yr <- as.vector(g.dflt(map.5yr[curncy], "5YR"))
    gap      <- as.vector(g.dflt(map.gap[curncy], "GAP"))
    tres.10y <- as.vector(g.dflt(map.10y[curncy], "10Y"))
    
    stdt <- g.date.idx(y$date[i], TRUE, +1)
    symbols <- paste(c(curncy, curncy %&% " BGNL", int.rate, vol, tres.5yr, tres.10y),
                     c("CURNCY", "CURNCY", "INDEX", "INDEX", "INDEX", "INDEX"))
    dta <- getBbHistorical(stdt, endt, symbol=symbols, pod=pod)
    if (is.null(dta) || !nrow(dta)) next
    if (is.na(dta[1, 3]))                   # When London was closed, try getting real-time LIBOR
      dta[1, 3] <- as.double(getBbRealTime("last_price", symbol=symbols[3], pod=pod))
    if (is.na(dta[1, 5]) && curncy=="NZD") {cat("Interpolating GNZGB5.\n")
                                            dta[1, 5] <- mean(cyld(c("GNZGB2","GNZGB7")))}

    ## Get PPP data.
    ppp.date <- g.date.idx( , TRUE, -1, cal="cal.year.end")
    ppp.dta <- getBbHistorical(ppp.date, ppp.date, symbol=ppp, type="INDEX", pod=pod)
    ppp.col <- if (is.null(ppp.dta) || !nrow(ppp.dta)) {
        g.dflt(y.ppp$ppp[y.ppp$curncy == curncy], NA_real_)
    } else {
        as.vector(ppp.dta)
    }
    ppp.col <- rep.int(ppp.col, nrow(dta))

    ## Get GAP data.
    gap.date <- g.date.idx( , TRUE, -1, cal="cal.qtr.end")
    gap.dta <- getBbHistorical(gap.date, gap.date, symbol=gap, type="INDEX", pod=pod)
    gap.col <- if (is.null(gap.dta) || !nrow(gap.dta)) {
        g.dflt(y.gap$gap[y.gap$curncy == curncy], NA_real_)
    } else {
        as.vector(gap.dta)
    }
    gap.col <- rep.int(gap.col, nrow(dta))
    
    dta <- cbind(dta, ppp=ppp.col, gap=gap.col)
    colnames(dta) <- c("fx", "fx_ldn", "i1", "ivol", "yld", "yld10", "ppp", "gap")
    dta <- as.data.frame(dta)
    dta$i2 <- NA_real_
    dta$date <- as.double(rownames(dta))
    dta$curncy <- curncy
    col.order <- c("date","curncy","fx","fx_ldn","i1","i2","ivol","ppp","yld","gap","yld10")
    dta <- subset(dta, !is.na(fx) | !is.na(i1), select=col.order)
    rownames(dta) <- NULL
    
    if (NROW(dta)) g.put.sql(dta, "gma..curncy", append=TRUE)
}
cat("\n")

## Export to "curncy" object:
stdt.export <- g.date.idx(endt, TRUE, -HISTORY.WINDOW)
y <- db.sql("SELECT a.curncy, b.[date],",
            " CASE WHEN (a.invert = 1) THEN 1 / b.fx ELSE b.fx END [fx], b.i1",
            " FROM gma..curncy_info a LEFT JOIN gma..curncy b ON a.curncy=b.curncy",
            " WHERE b.[date] BETWEEN '${stdt.export}' AND '$endt' ORDER BY a.curncy, b.[date]")

## Get currency volatilities and merge with database data.
curncy.info <- db.sql("SELECT * FROM gma..curncy_info")
vol.ticks   <- curncy.info$curncy %&% "V1M Curncy"
vol.ticks <- setdiff(vol.ticks, c("LKRV1M","PKRV1M","USDV1M","VEBV1M","MURV1M") %&% " Curncy")
bb <- getBbHistorical(endt, endt, "last_price", vol.ticks, pod=pod)
bb <- bb[g.dow(rownames(bb)) %in% c("Mon","Tue","Wed","Thu","Fri"), , drop=FALSE]
colnames(bb) <- substr(colnames(bb), 1, 3)
bb.df        <- data.frame(curncy=colnames(bb), ivol=c(bb))
bb.df$date   <- as.double(rownames(bb))

curncy <- merge(y, bb.df, by=c("curncy", "date"), all.x=TRUE)
save(curncy, file=g.p("$ddir/curncy.RData"), compress="xz")

## INDEX:
y <- db.sql("SELECT [index], max([date]) [date] FROM gma..[index] GROUP BY [index]")
y <- subset(y, date < endt)
y <- subset(y, index %ni% c("lf89truu","legatruu","djaig"))    # Gone

for (i in 1 %:% nrow(y)) {
    cat("[", index <- y$index[i], "] ", sep="")
    stdt  <- g.date.idx(y$date[i], TRUE, +1)
    dta <- getBbHistorical(stdt, endt, "last_trade", index, "INDEX", pod)
    if (is.null(dta) || !nrow(dta)) next
    colnames(dta) <- sub(" INDEX", "", colnames(dta))
    a <- g.array.to.ragged(dta, keys=c("date","index"), item="close")
    g.put.sql(a, "gma..index", append=TRUE)
}
cat("\n")

## Export to "index" object:
stdt.export <- g.date.idx(endt, TRUE, -HISTORY.WINDOW)
index <- db.sql("SELECT * FROM gma..[index]",
                " WHERE [date] BETWEEN '${stdt.export}' AND '$endt'",
                " ORDER BY [index], [date]")
index <- subset(index, index %ni% c("lf89truu","legatruu","djaig"))                        # Gone
save(index, file=g.p("$ddir/index.RData"), compress="xz")

## ETF:
y <- db.sql("SELECT symbol, max(date) 'date' FROM gma..etf GROUP BY symbol ORDER BY symbol")
y <- subset(y, date < endt)

for (i in 1 %:% nrow(y)) {
    cat("[", sec <- y$symbol[i], "] ", sep="")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
    dta  <- getETFData(stdt, sec, pod)
    if (!is.null(dta) && nrow(dta) > 0) g.put.sql(dta, "gma..etf", append=TRUE)
}
cat("\n")

## Export to "etf" object:
stdt.export <- g.date.idx(endt, TRUE, -HISTORY.WINDOW)
etf <- db.sql("SELECT date, symbol, price, tret, yield FROM gma..etf",
              " WHERE [date] BETWEEN '${stdt.export}' AND '$endt' ORDER BY [symbol], [date]")
save(etf, file=g.p("$ddir/etf.RData"), compress="xz")

## RISKFREE:
## Date for which we have the most recent value:
mxdt <- db.sql("SELECT geode.dbo.gdate8(max([date])) [date] FROM gma..riskfree")[[1]]
if (mxdt < endt) {
    stdt <- g.date.idx(mxdt, TRUE, +1)
    dta <- getBbHistorical(stdt, endt, symbol=c("USGG1M","USGG3M"), type="INDEX", pod=pod)
    colnames(dta) <- substr(colnames(dta),1,6)
    res  <- data.frame(date=as.double(rownames(dta)), dta); rownames(res) <- NULL
    g.put.sql(res, "gma..riskfree", append=TRUE)
}

## DIVYIELD:
## Date for which we have the most recent value:
mxdt <- db.sql("SELECT geode.dbo.gdate8(max([date])) [date] FROM gma..div_yield")[[1]]
if (mxdt < endt) {
    flds <- c("idx_est_dvd_yld", "eqy_dvd_yld_12m")
    bb <- getBbHistorical(mxdt, endt, flds, "SPX Index", pod=pod, dropSymbolDim=TRUE)
    if (!nrow(bb)) stop("No Bloomberg data for dividend yield")
    a <- subset(data.frame(date=as.double(rownames(bb)), bb), date > mxdt)
    if (nrow(a) > 0) g.put.sql(a, "gma..div_yield", append=TRUE)
}

## GAP:
## Update the gap_pred table w/ current and 4 quarters of predicted gap values for G10 countries.
stdt <- g.date.idx( , TRUE, -1, cal="cal.qtr.end")

## Create mapping of currencies to 
gap.map <- data.frame(curncy=c("USD","CAD","JPY","EUR","NOK","SEK","AUD","NZD","GBP","CHF"),
                      symbol=c("EOUSG001","EOCAG001","EOJPG001","E23EG001","EONOG001",
                               "EOSEG001","EOAUG001","EONZG001","EOUKG001","EOCHG001"))

## Determine the most recent date for which each country has data.
y <- db.sql("SELECT curncy, max(date) [date] FROM gma..gap_pred GROUP BY curncy")
y <- merge(y, gap.map, by="curncy", all=TRUE)
y$date[is.na(y$date)] <- g.date.idx( ,TRUE, -1)               # If not in table, start with today
curs <- subset(y, date < g.date8())$curncy
if (!length(curs)) {cat("Up to date.\n"); q(save="no")}        # Quit if everything is up-to-date

## Get end date of previous, current, and subsequent 3 periods, and label them 0, 1, 2, 3, 4.
periods <- sapply(structure(0:4, names=0:4), function(i)
                  g.date.idx(stdt, TRUE, +i, cal="cal.qtr.end", later=FALSE))
## periods <- structure(g.Date(periods), names=names(periods))

## Retrieve the data from BB for each security and insert it into the result matrix.
gap.mat <- g.mk.array(periods, curs, data=NA_real_)
for (i in curs) {
    cat("[", symbol <- gap.map$symbol[gap.map$curncy == i], "] ", sep="")
    for (j in periods) {
        gap.data <- getBbHistorical(j, j, symbol=symbol, type="INDEX", pod=pod)
        if (!is.null(gap.data) && nrow(gap.data)) gap.mat[as.character(j), i] <- gap.data
    }
}

## Swiss Data (CHF) only released yearly so fill forward each year.
for (i in 1:3) gap.mat[, "CHF"] <- g.dflt(gap.mat[, "CHF"], g.lag(gap.mat[, "CHF"]))

## Convert to long form and reorder
rownames(gap.mat) <- names(periods)[match(rownames(gap.mat), periods)]
gap.df <- g.array.to.ragged(gap.mat, keys=c("period","curncy"), item="gap")
gap.df$date <- g.date8()
class(gap.df$period) <- "integer"
gap.df <- gap.df[, c("date","curncy","period","gap")]

if (!g.exists.sql("gma.dbo.gap_pred")) {
    sql <- paste("CREATE TABLE gma.[dbo].gap_pred",
                 "(date datetime, curncy varchar(3), period int, gap float,",
                 "PRIMARY KEY CLUSTERED (date, curncy, period))")
    g.query.sql(sql)
}
g.put.sql(gap.df, "gma.dbo.gap_pred", append=TRUE)
