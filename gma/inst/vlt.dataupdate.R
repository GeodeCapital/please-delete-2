suppressMessages({library(gma); library(bbdotnet3)})
options(stringsAsFactors=FALSE, warn=1)

source(system.file("vlt.adddata.R", package="gma"))
source(system.file("idx.adddata.R", package="gma"))
source(system.file("mf.adddata.R", package="gma"))
## Sys.setenv(DBDSN="ciq", DBUSER="gcmquant", DBPWD="Plusm1nusSQL")
## g.connect.sql()
rconn <- g.connect.sql("ciq", "gcmquant", "Plusm1nusSQL", global=F)       # prod
dconn <- g.connect.sql("ciqdev", "gcmquant", "Plusm1nusSQL", global=F)    # dev

endt <- g.date.idx( , TRUE, -1)
pod <- tryPod(c("SB","FB"))



#################### commodities implied vol  #################### 
y <- db.sql("SELECT a.optn [optn], COALESCE(max(b.date), '19730102') [date]",
            " FROM res.dbo.ivcm_info a LEFT JOIN res.dbo.ivcm b ON a.optn=b.optn GROUP BY a.optn",
            conn=rconn)
y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -21))        # Need data but not dead


for (i in 1%:%nrow(y)) {                                                         # Get new data
    optn <- y$optn[i]
    cat(optn, "\n")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
                                        # test to see if the ticker exists
    testtk <- optn
    idx <- nchar(optn) == 1                        
    if(any(idx)) testtk[idx] <- optn[idx] %&% " " 
    bbtk <- paste(testtk %&% "1", "Comdty")
    existtk <- getBbHistorical(stdt, endt, "fut_cur_gen_ticker",bbtk, pod=pod)
    if (is.null(existtk) || !nrow(existtk)) {
        ## if ticker does not exist, then skip
    } else {
        if (optn %in% c('LA','LP','LX')) lme <- 1 else lme <- 0
        a <- add.ivdata(stdt, endt, optn, "Comdty", lme, pod)  ## data grabbing
        
        dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.ivcm WHERE optn='${optn}' ",
                          "AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   ## detect duplicates
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.ivcm", append=TRUE, conn=rconn)  ## update database

        dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.ivcm WHERE optn='${optn}' ",
                          "AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   ## detect duplicates
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.ivcm", append=TRUE, conn=dconn)  ## update database
    }
}


#################### equity implied vol #################### 
y <- db.sql("SELECT a.optn [optn], COALESCE(max(b.date), '19730102') [date]",
            " FROM res.dbo.iveq_info a LEFT JOIN res.dbo.iveq b ON a.optn=b.optn GROUP BY a.optn",
            conn=rconn)
y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -21))        # Need data but not dead


for (i in 1%:%nrow(y)) {                                                         # Get new data
    optn <- y$optn[i]
    cat(optn, "\n")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
                                        # test to see if the ticker exists
    testtk <- optn
    idx <- nchar(optn) == 1                        
    if(any(idx)) testtk[idx] <- optn[idx] %&% " " 
    bbtk <- paste(testtk, "Index")
    existtk <- getBbHistorical(stdt, endt, "px_last", bbtk, pod=pod)
    if (nrow(existtk)==0) {
        ## if ticker does not exist, then skip
    } else {
        a <- add.ivdata(stdt, endt, optn, "Index", 0, pod)  ## data grabbing
        
        dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.iveq WHERE optn='${optn}' ",
                          "AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   ## detect duplicates
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.iveq", append=TRUE, conn=rconn)  ## update database

        dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.iveq WHERE optn='${optn}' ",
                          "AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   ## detect duplicates
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.iveq", append=TRUE, conn=dconn)  ## update database
    }
}


#################### bond implied vol #################### 
y <- db.sql("SELECT a.optn [optn], COALESCE(max(b.date), '19730102') [date]",
            " FROM res.dbo.ivbd_info a LEFT JOIN res.dbo.ivbd b ON a.optn=b.optn GROUP BY a.optn",
            conn=rconn)
y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -21))        # Need data but not dead


for (i in 1%:%nrow(y)) {                                                         # Get new data
    optn <- y$optn[i]
    cat(optn, "\n")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
                                        # test to see if the ticker exists
    testtk <- optn
    idx <- nchar(optn) == 1                        
    if(any(idx)) testtk[idx] <- optn[idx] %&% " " 
    bbtk <- paste(testtk %&% "1", "Comdty")
    existtk <- getBbHistorical(stdt, endt, "fut_cur_gen_ticker",bbtk, pod=pod)
    if (is.null(existtk) || !nrow(existtk)) {
        ## if ticker does not exist, then skip
    } else {
        a <- add.ivdata(stdt, endt, optn, "Comdty", 0, pod)  ## data grabbing
        
        dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.ivbd WHERE optn='${optn}' ",
                          "AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   ## detect duplicates
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.ivbd", append=TRUE, conn=rconn)  ## update database

        dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.ivbd WHERE optn='${optn}' ",
                          "AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   ## detect duplicates
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.ivbd", append=TRUE, conn=dconn)  ## update database
    }
  }


#################### more OTC implied vol #################### 
y <- db.sql("SELECT name, assetclass, max(date) as date from res.dbo.ctry_iv_bb ",
            "group by name, assetclass order by assetclass", conn=rconn)
y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -60))        # Need data but not dead

for (i in 1%:%nrow(y)) {
    optn <- y$name[i]
    cat(optn, "\n")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
    ## test to see if the ticker exists
    testtk <- optn
    idx <- nchar(optn) == 1                        
    if(any(idx)) testtk[idx] <- optn[idx] %&% " "
    if (y$assetclass[i] == "FX") {
        existtk <- y[i, ]
    } else {
        bbtk <- paste(testtk %&% "1", "Comdty")
        existtk <- getBbHistorical(stdt, endt, "fut_cur_gen_ticker",bbtk, pod=pod)
    } 
    if (is.null(existtk) || !nrow(existtk)) {
        ## if ticker does not exist, then skip
    } else {
        fld <- switch(y$assetclass[i],
                      FX='px_last', 
                      BD=c(m1="30Day_IMPVOL_100.0%MNY_DF", m3="3MTH_IMPVOL_100.0%MNY_DF"))
        if (y$assetclass[i] == 'FX') {
             a1 <- getBbHistorical(stdt, endt, fld,
                                   paste("USD", optn, "V", "1M", " BGN Curncy", sep=""), pod=pod)
             a2 <- getBbHistorical(stdt, endt, fld,
                                   paste("USD", optn, "V", "3M", " BGN Curncy", sep=""), pod=pod)
             a3 <- getBbHistorical(stdt, endt, fld,
                                   paste("USD", optn, "V", "1Y", " BGN Curncy", sep=""), pod=pod)
             a <- cbind(a1, a2, a3)
             colnames(a) <- c('m1', 'm3', 'm12')
             a <- g.array.to.ragged(a)
             colnames(a) <- c('date','maturity','data')
        } else {
            a <- getBbHistorical(stdt, endt, fld, existtk[1,1], "Comdty", pod)
            dimnames(a)[[3]] <- c('m1','m3')
            a <- g.array.to.ragged(a)
            colnames(a) <- c('date','name','maturity','data')
        }
        a$date <- g.date8(a$date)
        a$name <- optn
        a$assetclass <- y$assetclass[i]
        a <- a[, c('date','name','maturity','data','assetclass')]
        
        dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.ctry_iv_bb WHERE name='${optn}' ",
                          "AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   ## detect duplicates
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.ctry_iv_bb", append=TRUE, conn=rconn)  # update database

        dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.ctry_iv_bb WHERE name='${optn}' ",
                          "AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   ## detect duplicates
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.ctry_iv_bb", append=TRUE, conn=dconn)  # update database
    }
}


#################### equity index data #################### 
y <- db.sql("SELECT a.idx [idx], COALESCE(max(b.date), '19730102') [date]",
            " FROM res.dbo.eqidx_info a LEFT JOIN res.dbo.eqidx b ON a.idx=b.idx GROUP BY a.idx", conn=rconn)
y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -21))        # Need data but not dead


for (i in 1%:%nrow(y)) {                                                         # Get new data
    idx <- y$idx[i]
    cat(idx, "\n")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
                                        # test to see if the ticker exists
    testtk <- idx
    itest <- nchar(idx) == 1                        
    if(any(itest)) testtk[itest] <- idx[itest] %&% " " 
    bbtk <- paste(testtk, "Index")
    existtk <- getBbHistorical(stdt, endt, "px_last", bbtk, pod=pod)
    if (nrow(existtk)==0) {
        ## if ticker does not exist, then skip
    } else {
        a <- add.idxdata(stdt, endt, idx, "Index", pod)  ## data grabbing
        
        if (idx == "RTSI$") {   ## detect duplicates
            dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.eqidx WHERE ",
                              "idx LIKE 'RTSI%' AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)
        } else {
            dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.eqidx WHERE ",
                              "idx='${idx}' AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   
        }
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.eqidx", append=TRUE, conn=rconn)  ## update database

        if (idx == "RTSI$") {   ## detect duplicates
            dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.eqidx WHERE ",
                              "idx LIKE 'RTSI%' AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)
        } else {
            dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.eqidx WHERE ",
                              "idx='${idx}' AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   
        }
        if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.eqidx", append=TRUE, conn=dconn)  ## update database
    }
}


#################### macro data - GDP #################### 
y <- db.sql("SELECT a.ctry [ctry], a.gdplevel [gdplevel], a.gdpqoq [gdpqoq], ",
            " a.gdpyoy [gdpyoy], max(b.date) [date]",
            " FROM res.dbo.macro_info a LEFT JOIN res.dbo.macro_data b ON a.ctry=b.ctry",
            " GROUP BY a.ctry, a.gdplevel, a.gdpqoq, a.gdpyoy", conn=rconn)

for (i in 1 %:% nrow(y)) {                                                         # Get new data
    ctry <- y$ctry[i]
    cat(ctry, "\n")
    cp.date <- g.date.idx( , TRUE, -1, cal="cal.qtr.end")
    co.date <- g.date.idx(cp.date, T, -2, cal="cal.qtr.end")
    a <- getBbHistorical(co.date, cp.date, "px_last", y[i, 2:(ncol(y)-1)], "Index", pod)
    a <- as.data.frame(a[nrow(a), ,drop =F])
    a$date <- endt
    rownames(a) <- NULL
    a$asset <- y[i, 1]
    colnames(a) <- c('gdplevel','gdpqoq','gdpyoy','date','ctry')
    a <- a[, c('date','ctry','gdplevel','gdpqoq','gdpyoy')]
    
    dup <- db.sql(g.p("SELECT * FROM res.dbo.macro_data WHERE ctry='${ctry}' ",
                      "AND date >= '${cp.date}' AND date <= '${endt}'"), conn=rconn)   ## detect duplicates
    a.new <- a[!(g.date8(a$date) %in% dup$date), ]
    if (nrow(dup)!=0) {
        a.new[is.na(a.new)] <- dup[nrow(dup), is.na(a.new)]
    }
    if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.macro_data", append=TRUE, conn=rconn)

    dup <- db.sql(g.p("SELECT * FROM res.dbo.macro_data WHERE ctry='${ctry}' ",
                      "AND date >= '${cp.date}' AND date <= '${endt}'"), conn=dconn)   ## detect duplicates
    a.new <- a[!(g.date8(a$date) %in% dup$date), ]
    if (nrow(dup)!=0) {
        a.new[is.na(a.new)] <- dup[nrow(dup), is.na(a.new)]
    }
    if (nrow(a.new)!=0) g.put.sql(a.new, "res.dbo.macro_data", append=TRUE, conn=dconn)
}


#################### bond futures data #################### 
y <- db.sql("SELECT a.bond [bond], a.yield [yield], COALESCE(max(b.date), '19730102') [date]",
            " FROM res.dbo.mfbd_info a LEFT JOIN res.dbo.mfbd b ON a.bond=b.bond GROUP BY a.bond, a.yield",
            conn=rconn)
y <- subset(y, bond %ni% c("ES","L"))
y <- subset(y, date < endt & date >= g.date.idx(endt, TRUE, -5))        # Need data but not dead
no.adj.pod <- if (all(g.date.idx(y$date, T, +1) == endt)) pod else tryPod(NO.ROLL.ADJUST.POD)
if (identical(no.adj.pod, FALSE)) stop("Could not connect to an unadjusted pod!")

stir <- c('ER', 'L', 'ED', 'BA', 'IR')     # short term interest rate futures

for (i in 1 %:% nrow(y)) {                                                         # Get new data
    bond <- y$bond[i]
    cat(bond, "\n")
    stdt <- g.date.idx(y$date[i], TRUE, +1)
    # test to see if the ticker exists
    testtk <- bond
    idx <- nchar(bond) == 1                        
    if(any(idx)) testtk[idx] <- bond[idx] %&% " " 
    bbtk <- paste(testtk %&% "1", "Comdty")
    existtk <- getBbHistorical(stdt, endt, "fut_cur_gen_ticker",bbtk, pod=no.adj.pod)
    if (is.null(existtk) || !nrow(existtk)) {
      ## if ticker does not exist, then skip
    } else {
      if (bond %in% stir) {
          suffixes <- 1:10
      } else {
          suffixes <- 1:2
      }
      a <- add.data(stdt, endt, bond, "bond", "Comdty",
                    y$yield[i], suffixes, pod, no.adj.pod)  ## data grabbing
      dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.mfbd WHERE bond='${bond}' ",
                        "AND date > '${stdt}' AND date < '${endt}'"), conn=rconn)   ## detect duplicates
      if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.mfbd", append=TRUE, conn=rconn)  ## update database
      dup <- db.sql(g.p("SELECT COUNT(*) FROM res.dbo.mfbd WHERE bond='${bond}' ",
                        "AND date > '${stdt}' AND date < '${endt}'"), conn=dconn)   ## detect duplicates
      if (!is.null(a) && dup==0) g.put.sql(a, "res.dbo.mfbd", append=TRUE, conn=dconn)  ## update database
    }
}
