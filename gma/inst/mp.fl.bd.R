require(gma)
require(zoo)
require(parallel)
require(analytics)
options(stringsAsFactors=FALSE, warn=1)

rconn <- g.connect.sql("ciq", "gcmquant", "Plusm1nusSQL", global=F)
gconn <- g.connect.sql(global=F)

dir <- "/gcm/strategies/gma/mp/fl/bd/"
existd <- g.date8(GetRddDates(bDir = dir,
                              fName = "bd.sigm.RData"))
cdate <- g.date.idx(,T,)         # folder date that saves up to eod yesterday data
sdt <- ifelse(length(existd)==0, 19930104, g.date.idx(tail(existd,1),T,1))
dts <- dates.usd[g.inorder(sdt, dates.usd, cdate)]

for (n in 1:length(dts)) {
    nowdate <- dts[n]
    cat(nowdate, "\n")
    siglist <- mp.fl.bd.sig(nowdate)

    bd.sigm <- siglist[['sig']]
    bd.sigw <- siglist[['w']]
    system(paste("mkdir -p", m.rdd.path(date = nowdate, dir = dir)))
    save(bd.sigm, file=m.rdd.path(date = nowdate, dir = dir, file = "bd.sigm.RData"))
    save(bd.sigw, file=m.rdd.path(date = nowdate, dir = dir, file = "bd.sigw.RData"))
}

######### performance
## directory and dates
dir <- "/gcm/strategies/gma/mp/fl/bd/"
existd <- g.date8(GetRddDates(bDir = dir,
                              fName = "bd.sigm.RData"))
stdt <- existd[1]
endt <- tail(existd, 1)

## bd returns
bd <- c(CA='CN',GB='G',IT='IK',JP='JB',FR='OAT',DE='RX',US='TY',AU='XM')
bdd <- mp.getfutures(bd, stdt, endt, "bond")
bdr <- (bdd[2:nrow(bdd),,1,'adjprc'] - bdd[1:(nrow(bdd)-1),,1,'adjprc']) /
    bdd[1:(nrow(bdd)-1),,1,'price']
ctry <- c(CN='CAN',G='GBR',IK='ITA',JB='JPN',OAT='FRA',RX='DEU',TY='USA',XM='AUS')

## signal weights - won't run before EUR becomes available
bd.sigw <- get(load(m.rdd.path(date = existd[1],
                               dir = dir, file = "bd.sigw.RData")))
w <- array(NA, c(length(existd), ncol(bdr), ncol(bd.sigw)),
           list(existd, colnames(bdr), colnames(bd.sigw)))
for (n in 1:length(existd)) {
    et <- existd[n]     # by folder date
    bd.sigw <- get(load(m.rdd.path(date = et,
                                   dir = dir, file = "bd.sigw.RData")))
    w[n, rownames(bd.sigw), ] <- bd.sigw    # weights availabe that day: based on ystd data
}
w[is.na(w)] <- 100   # set NA to some random number to be NA'ed later

## turn daily weights to monthly
mnth <- unique(g.date.idx(rownames(w), T, 0, T, 'mnth.beg'))
mnth <- g.date.idx(mnth, T, 2, T)     # avoid 1st bday of the month
wm <- w; wm[] <- NA
for (n in 1:length(mnth)) {
    mn <- mnth[n]
    wm[which(rownames(wm)==mn),,] <- w[which(rownames(w)==mn),,]
}
for (m in 1:dim(wm)[[3]]) {
    wm[,,m] <- g.nafill(wm[,,m], maxfill=63)
}
wm[wm==100] <- NA  # take out random number

## signal portfolio
sigr <- array(NA, c(nrow(bdr), ncol(bd.sigw)),
             list(rownames(bdr), colnames(bd.sigw)))
for (n in 1:ncol(sigr)) {
    sigr.t <- rowSums(bdr * wm[1:(nrow(wm)-2),, n], na.rm=T)
    sigr[rownames(sigr) %in% names(sigr.t), n] <- sigr.t
}
sigr[sigr==0] <- NA

## aggregate signals
wm.t <- wm; wm.t[is.na(wm.t)] <- 0
for (n in 1:nrow(wm.t)) {
    na.test <- colSums(abs(wm.t[n,,]))
    wm.t[n, , which(na.test==0)] <- NA
}
wm.t <- wm.t[1:(nrow(wm.t)-2), , ]
allr <- cbind(pfr=rowMeans(sigr, na.rm=T) / rowSums(abs(apply(wm.t, c(1,2), mean, na.rm=T)), na.rm=T), sigr)

## plot
lastdt <- rownames(allr)[nrow(allr)]
lastyr <- g.date.idx(lastdt, T, -1, T, 'year.end')
lastmn <- g.date.idx(lastdt, T, -1, T, 'mnth.end')
g.printer(file= g.p(dir, "performance.pdf"))
par(mfrow=c(3,1))
for (n in 1:ncol(allr)) {
    g.plot.perf(allr[, n], annlz=252,
                title=g.p(colnames(allr)[n], " FS as of ", lastdt),
                stats=c("gr","sd","gir","dd","skew","kurtosis"))
    tmp <- allr[rownames(allr)>=lastyr, n]
    if (length(tmp)>3) {
        g.plot.perf(tmp, annlz=length(tmp),
                    title=g.p(colnames(allr)[n], " YTD as of ", lastdt),
                    stats=c("gr","sd","gir","dd","skew","kurtosis"))
    }
    tmp <- allr[rownames(allr)>=lastmn, n]
    if (length(tmp)>3) {
        g.plot.perf(tmp, annlz=length(tmp),
                    title=g.p(colnames(allr)[n], " MTD as of ", lastdt),
                    stats=c("gr","sd","gir","dd","skew","kurtosis"))
    }
}
dev.off()
