library(zoo)
library(analytics)
library(gma)
options(stringsAsFactors=FALSE, warn=1)

######### date set up #########
##### get the last date in the directory
existd <- g.date8(GetRddDates(bDir = "/gcm/gcm_strategies/gma/marb",
                              fName = "maret.RData"))              # all folders available
curdate <- g.date.idx(,T,)                                         # current date
alld <- dates.usd[g.inorder(g.date.idx(tail(existd,1),T,1),
                            dates.usd, curdate)]                   # all dates available
orgd <- dates.usd[g.inorder(20171229, dates.usd, curdate)]   # same as alld with diff purpose

dofweek <- weekdays(g.Date(orgd))
nreb <- which(dofweek == "Wednesday")          # rebalance on Wednesday
rebd <- orgd[nreb]

##### Change rebal date to Thursdays for Wednesdays that fall on July 4th, Christmas, New Years
yrl <- unique(format(g.Date(orgd), "%Y"))
mdl <- c("0101", "0704", "1225")
allhol <- array(data=NA, dim=c(1, length(yrl)*length(mdl)),
                dimnames=list("hld", 1:(length(yrl)*length(mdl))))
for (x in 1:length(yrl)){
    allhol[(x*3-2):(x*3)] <- paste(yrl[x], mdl, sep="")          # all possible dates
}
allhol <- allhol[allhol<curdate]
anywed <- allhol[which(weekdays(g.Date(allhol)) == "Wednesday")]         # any Wednesdays
if (length(anywed) != 0) {
    altday <- g.date.idx(g.date8(anywed), T, 1, cal="cal")               # Thursdays
    rebd <- sort(c(rebd, altday))
}

######### main section  ######### 
for (kkx in 1 %:% length(alld)) {
    nowdate <- alld[kkx]
    cat(alld[kkx], "\n")

    ## find last reb date
    endt <- tail(rebd[rebd < nowdate], 1)

    source(system.file("mp.marb.signal.R", package="gma"))         # signal code

    g.clean(keep=c("alld", "rebd"))     # keep it clean
}
