## Fill late-arrival openint data for CC, CO, CT, KC, KW, QC, QS, SB; available by 9:45
## I do not expect data for LMxx, LL, LP, LT, LY, O, PA, PL, RR
suppressMessages({library(gma); library(bbdotnet3)})
pod <- tryPod(PODS)
## Sys.setenv(DBDSN="prod", DBUSER="gcmgma", DBPWD=Sys.getenv("DBGMA"))    # If you aren't gcmgma
g.connect.sql()
pdt <- g.date.idx( ,T,-3)
mlet <- c("F","G","H","J","K","M","N","Q","U","V","X","Z")
     
ctxt <- "('CC','CO','CT','KC','KW','QC','QS','SB','LA','LN','LX'," %&% # Cmdtys with late OI data
        " 'BO','C', 'CL','FC','GC','HG','HO','LC','LH','NG','S', 'SI','SM','W', 'XB')" # Us'ly OK
x <- db.sql("select * from gma..cmdty where date >= '$pdt' and openint is null",
            " and cmdty in $ctxt order by date, cmdty, cnum")
for (i in 1 %:% nrow(x)) {
    dt <- x$date[i]
    symb <- x$cmdty[i] %&% c(" ","","","")[nchar(x$cmdty[i])] %&% x$cnum[i] %&% " Comdty"
    bb <- getBbHistorical(dt, dt, "open_int", symb, pod=pod, output="table")
    if (is.null(bb) || !nrow(bb) || is.na(bb$open_int)) next
    cat("Filling openint for", dt, symb, bb$open_int, "\n")
    g.query.sql(g.p("update gma..cmdty set openint=$val where date='$dt' and cmdty='$cmdty'",
                    " and cnum=$cnum", cmdty=x$cmdty[i], cnum=x$cnum[i], val=bb$open_int))
}

## Similarly for the djubs table:
x <- db.sql("select * from gma..djubs where date >= '$pdt' and open_int is null",
            " and substring(ticker,1,2) in $ctxt order by date,ticker")
for (i in 1 %:% nrow(x)) {
    dt <- x$date[i]
    symb <- sub("(...).(.)", "\\1\\2", x$ticker[i]) %&% " Comdty"

    yrmo <- 100*as.numeric(substr(x$ticker[i], 4,5)) + match(substr(x$ticker[i],3,3), mlet)
    if ((substr(x$ticker[i], 1,2)=="LA" & yrmo >= 1711) ||
        (substr(x$ticker[i], 1,2)=="NG" & yrmo >= 1803)) symb <- x$ticker[i] %&% " Comdty"

    bb <- getBbHistorical(dt, dt, "open_int", symb, pod=pod, output="table")
    if (is.null(bb) || !nrow(bb) || is.na(bb$open_int)) next
    cat("Filling openint for", dt, symb, bb$open_int, "\n")
    g.query.sql(g.p("update gma..djubs set open_int=$val where date='$dt' and ticker='$tick'",
                    tick=x$ticker[i], val=bb$open_int))
}


## equity and commodities fillin for the ciq database
rconn <- g.connect.sql("ciq", "gcmquant", "Plusm1nusSQL", global=F)

eqn <- db.sql('select eqty from res..mfeq_info', conn=rconn)
bdn <- db.sql('select bond from res..mfbd_info', conn=rconn)

## equity fill in
ctxt <- paste(sQuote(eqn$eqty), collapse=",")
x <- db.sql("select * from res..mfeq where date >= '$pdt' and (openint is null or volume is null)",
            " and eqty in ($ctxt) order by date, eqty, cnum", conn=rconn)
for (i in 1 %:% nrow(x)) {
    dt <- x$date[i]
    symb <- x$eqty[i] %&% c(" ","","","")[nchar(x$eqty[i])] %&% x$cnum[i] %&% " Index"
    bb <- getBbHistorical(dt, dt, c("open_int", "volume"), symb, pod=pod, output="table")
    if (nrow(bb)==0 || is.na(bb$open_int) || is.na(bb$volume)) next
    cat("Filling openint for", dt, symb, bb$open_int, bb$volume, "\n")
    g.query.sql(g.p("update res..mfeq set openint=$valo, volume=$valv where date='$dt' and eqty='$eqty'",
                 " and cnum=$cnum", eqty=x$eqty[i], cnum=x$cnum[i], valo=bb$open_int, valv=bb$volume), conn=rconn)
}

## bond fill in
ctxt <- paste(sQuote(bdn$bond), collapse=",")
x <- db.sql("select * from res..mfbd where date >= '$pdt' and (openint is null or volume is null)",
            " and bond in ($ctxt) order by date, bond, cnum", conn=rconn)
for (i in 1 %:% nrow(x)) {
    dt <- x$date[i]
    symb <- x$bond[i] %&% c(" ","","","")[nchar(x$bond[i])] %&% x$cnum[i] %&% " Comdty"
    bb <- getBbHistorical(dt, dt, c("open_int", "volume"), symb, pod=pod, output="table")
    if (nrow(bb)==0 || is.na(bb$open_int) || is.na(bb$volume)) next
    cat("Filling openint for", dt, symb, bb$open_int, bb$volume, "\n")
    g.query.sql(g.p("update res..mfbd set openint=$valo, volume=$valv where date='$dt' and bond='$bond'",
                 " and cnum=$cnum", bond=x$bond[i], cnum=x$cnum[i], valo=bb$open_int, valv=bb$volume), conn=rconn)
}
