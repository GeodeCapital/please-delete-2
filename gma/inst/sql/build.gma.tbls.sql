/*
Geode Capital Management (2010)
Daniel Gerlanc
Code to generate SQL Tables in the GMA database
*/

USE gma

/* Commodity Tables */

CREATE TABLE [dbo].cmdty_info (
  cmdty    varchar(255),
  term     varchar(2) NOT NULL,
  [name]   varchar(255),
  grp      varchar(255),
  category varchar(255),
  PRIMARY KEY CLUSTERED (cmdty)
)

CREATE TABLE [dbo].cmdty (
  date   datetime     NOT NULL,
  cmdty  varchar(255) NOT NULL,
  cnum   int          NOT NULL,
  price  float,
  dprc   float,
  ticker varchar(255),
  expiry varchar(255),
  PRIMARY KEY CLUSTERED ([date], cmdty, cnum),
  FOREIGN KEY (cmdty) REFERENCES cmdty_info (cmdty)
)

CREATE TABLE [dbo].djubs (
  [date]   datetime,
  ticker   varchar(20),
  price    float,
  open_int int,
  volume   int
  PRIMARY KEY CLUSTERED ([date], ticker)
)

CREATE TABLE [dbo].cmdty_contract_override (
  cmdty  varchar(255),
  cnum1  int NOT NULL,
  cnum2  int NOT NULL,
  PRIMARY KEY CLUSTERED (cmdty),
  FOREIGN KEY (cmdty) REFERENCES cmdty_info (cmdty)
)

/* Currency Tables */

CREATE TABLE [dbo].curncy_info (
  curncy   varchar(255),
  invert   int,
  i1_index varchar(255),
  i2_index varchar(255)       	          
)

CREATE TABLE [dbo].curncy (
  date   datetime,
  curncy varchar(3),
  fx     float,
  fx_ldn float,
  i1     float,
  i2     float,
  ivol   float,
  ppp    float,
  yld    float,
  gap    float,
  PRIMARY KEY CLUSTERED ([date], curncy)
)

/* Misc Data */

CREATE TABLE [dbo].div_yield (
  [date] datetime PRIMARY KEY CLUSTERED,
  idx_est_dvd_yld float,
  eqy_dvd_yld_12m float
)

CREATE TABLE [dbo].etf (
  [date] datetime,
  symbol varchar(255),
  price  float,
  tret   float,
  yield  float,
  PRIMARY KEY CLUSTERED ([date], symbol)
)

CREATE TABLE [dbo].index (
  [date]  datetime,
  [index] varchar(255),
  [close] float,
  PRIMARY KEY CLUSTERED ([date], [index])
)

CREATE TABLE [dbo].gap_pred (
  [date]  datetime,
  curncy  varchar(3),
  period  int,
  gap     float,
  PRIMARY KEY ([date], curncy, period) 

CREATE TABLE [dbo].riskfree (
  [date] datetime PRIMARY KEY,
  USGG1M float,
  USGG3M float
)

/* Performance */

CREATE TABLE [dbo].rtperf (
  fund varchar(50) NOT NULL,
  bps  float
)

CREATE TABLE [dbo].histsim (
  [date]    datetime,
  rfd       float,
  curr_car  float,
  curr_val  float,
  curr_yld  float,
  curr_mom  float,
  curr_grt  float,
  cmdty_bak float,
  vlty_spx  float
)

/* Holdings */
CREATE TABLE [dbo].cur_holding_sod (
  [date]    datetime,
  fund      varchar(255),
  cusip     varchar(9),
  symbol    varchar(255),
  shares    float,
  price     float,
  mkt_value float,
  w_ls      float,
  w_eq      float,
  src       varchar(255),
)

CREATE TABLE [dbo].hsod (
  [date]    float,
  fund      varchar(255),
  symbol    varchar(255),
  id        varchar(255),
  shares    float,
  mult      float,
  pcls      float
)

/* GMA Metadata */
  
CREATE TABLE [dbo].tbl_info (
  tbl_name varchar(255),
  [desc]     varchar(255),
  PRIMARY KEY CLUSTERED (tbl_name)
)

CREATE TABLE [dbo].dtproperties (
  [id]       int,
  objectid   int,
  property   varchar(64),
  [value]    varchar(255),
  uvalue     nvarchar(255),
  lvalue     image,
  version    int,
  PRIMARY KEY ([id], property)
)